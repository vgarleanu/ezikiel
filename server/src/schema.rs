table! {
    nodes (id) {
        id -> Integer,
        hwid -> Text,
        ip -> Text,
        cl_version -> Integer,
    }
}
