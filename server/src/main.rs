#![feature(custom_attribute)]
#[macro_use]
extern crate dotenv;

#[macro_use]
extern crate serde_derive;

#[feature(uniform_paths)]
extern crate rmp_serde as rmps;

pub mod network {
    pub mod protocol;
}

use self::network::protocol::*;

fn main() {
    println!("Welcome to Ezikiel");
    let mut network_protocol = NetworkProtocol::new();
    println!("NetworkProtocol created!");
    let result = network_protocol.start("127.0.0.1:5050".to_string());
    println!("Starting server!");
}
