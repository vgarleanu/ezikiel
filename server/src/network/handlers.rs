pub struct ActionHandler {
    db_ctx: u16,
}

impl ActionHandler {
    pub fn new() -> Self {
        Self { db_ctx: 2 }
    }

    pub fn handle_announce(&mut self) -> Vec<u8> {
        println!("Handled announce");
        vec![0x00, 0x0f, 0x00, 0x00, 0x00, 0x23]
    }
}
