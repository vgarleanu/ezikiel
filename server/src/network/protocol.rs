// Protocol and server event loop module

extern crate serde;

// Use manual module paths because of bug with cargo
#[path = "./handlers.rs"]
pub mod handlers;

// TODO: Change self to crate, see E0503
use self::handlers::ActionHandler;
use rmps::{Deserializer, Serializer};
use serde::{Deserialize, Serialize};
use std::borrow::{Borrow, BorrowMut};
use std::io::{Error, ErrorKind, *};
use std::net::{TcpListener, TcpStream};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

pub struct NetworkProtocol {
    // ActionHandler, see core/handlers.rs
    // Stored in a shared objects use by the NetworkProtocol
    // object to handle specific actions specificed by Packet::Action
    action_handler: ActionHandler,
}

pub struct Packet {
    version: u16,
    packet_len: u16,
    // TODO: Change action to a enum(u16) instead of u16
    action: u16,
    payload: Vec<u8>,
}

// TODO: Make this use tokio::async
impl NetworkProtocol {
    pub fn new() -> NetworkProtocol {
        let action_handler = ActionHandler::new();

        NetworkProtocol {
            action_handler: action_handler,
        }
    }

    pub fn start(&mut self, addr: String) -> Result<()> {
        let listener = TcpListener::bind(addr)?;
        //let mut db_conn = Database::new();
        for stream in listener.incoming() {
            let stream_clone = stream.unwrap().try_clone().expect("Failed to clone stream");
            thread::spawn(move || {
                match NetworkProtocol::handle_connection(&stream_clone) {
                    Ok(_) => {}
                    Err(_) => {}
                };
            });
        }

        Ok(())
    }

    fn handle_packet(mut stream: &TcpStream, packet: Packet) -> Result<()> {
        let mut msg_answer = vec![0; 0];
        let mut action_handler = ActionHandler::new();
        msg_answer = match packet.action {
            1 => action_handler.handle_announce(),
            _ => return Err(Error::new(ErrorKind::Other, "Invalid action")),
        };
        stream.write(msg_answer.as_mut_slice());
        Ok(())
    }

    fn handle_connection(mut stream: &TcpStream) -> Result<()> {
        let mut version = [0; 2];
        let mut packet_len = [0; 2];
        let mut action = [0; 2];

        stream.read_exact(&mut version)?;
        stream.read_exact(&mut packet_len)?;
        stream.read_exact(&mut action)?;

        let version = u16::from_be_bytes(version);
        let packet_len = u16::from_be_bytes(packet_len);
        let action = u16::from_be_bytes(action);
        let mut payload = vec![0; packet_len as usize];

        if packet_len > 0 {
            stream.read_exact(&mut payload)?;
        }

        let mut packet = Packet {
            version: version,
            packet_len: packet_len,
            action: action,
            payload: payload,
        };

        println!(
            "V: {} PL: {} A: {}",
            packet.version, packet.packet_len, packet.action
        );
        match NetworkProtocol::handle_packet(stream, packet) {
            Ok(result) => NetworkProtocol::handle_connection(stream),
            Err(err) => return Err(err),
        }
    }
}
