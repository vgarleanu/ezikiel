use crate::schema::nodes;

#[derive(Queryable)]
pub struct Nodes {
    pub id: i32,
    pub hwid: String,
    pub ip: String,
    pub cl_version: i32,
}

#[derive(Insertable)]
#[table_name = "nodes"]
pub struct NewNode<'a> {
    pub hwid: &'a str,
    pub ip: &'a str,
    pub cl_version: &'a i32,
}
