#[macro_use]
use diesel::prelude::*;
use crate::models::*;
use diesel::sqlite::{Sqlite, SqliteConnection};
use diesel::Connection;
use dotenv::dotenv;
use std::env;
use std::sync::{Arc, Mutex};

#[derive(Debug, Copy, Clone)]
pub struct Database {
    db: Arc<Mutex<SqliteConnection>>,
}

impl Database {
    pub fn new() -> Arc<Mutex<Database>> {
        dotenv().ok();

        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        Arc::new(Mutex::new(Database {
            db: Arc::new(Mutex::new(
                SqliteConnection::establish(&database_url).expect("error connecting to db"),
            )),
        }))
    }

    pub fn ping(&self) {
        println!("Pinged db");
    }
}
