#include <iostream>
#include <memory>
#include "network/network.h"
#include "core/handler.h"

int main(){
    /*
     * Announce ourselves to the server using the
     * packet action ANNOUNCE
     */
    std::unique_ptr<Network> netinst = std::make_unique<Network>();
    std::unique_ptr<NetworkPacket> annpacket = std::make_unique<NetworkPacket>(Actions(ANNOUNCE));
    netinst->send_packet(std::move(annpacket));


    std::unique_ptr<Handler> handler_loop = std::make_unique<Handler>();
    for(size_t i = 0; i<5; i++){
        auto to_send = handler_loop->handle(std::move(netinst->recv_packet()));
        netinst->send_packet(std::move(to_send));
    }
}
