#include <memory>
#include "core/handler.h"
#include "network/network.h"
#include "network/message.h"

std::unique_ptr<NetworkPacket> Handler::handle(std::unique_ptr<NetworkPacket> np){
    switch(np->action){
        case ANNOUNCE:
            return this->handle_announce(std::move(np));
        default:
            return this->handle_announce(std::move(np));
    }
}

std::unique_ptr<NetworkPacket> Handler::handle_announce(std::unique_ptr<NetworkPacket> np){
    if(np->packet_len > 0){
        std::unique_ptr<Message> msg = Message::unpack_char(np->payload);
        auto contained = msg->data.find("token");
        if(contained != msg->data.end()){
            const char* payload = msg->pack_char();
            std::cout << "Token announced" << std::endl;
            this->token = contained->second;
            std::unique_ptr<NetworkPacket> new_packet = std::make_unique<NetworkPacket>(1, np->packet_len, (char*)payload);
            return new_packet;
        }
    }

    std::unique_ptr<NetworkPacket> new_packet = std::make_unique<NetworkPacket>(1, 0, 1);
    return new_packet;
}
