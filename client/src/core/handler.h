#ifndef HANDLER_H
#define HANDLER_H
#include "network/network.h"
#include <string>
#include <memory>

class Handler{
    public:
        std::unique_ptr<NetworkPacket> handle(std::unique_ptr<NetworkPacket> np);
    private:
        std::unique_ptr<NetworkPacket> handle_announce(std::unique_ptr<NetworkPacket> np);
        std::string token;
};

#endif
