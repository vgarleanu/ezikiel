#include "network.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define PORT 5050
const int i = 1;
#define is_bigendian() ( (*(char*)&i) == 0)

Network::Network(){
    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1){
        printf("Failed to create socket\n");
        exit(0);
    }

    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);

    if(connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) != 0){
        printf("Connection failed\n");
        exit(0);
    }
    this->sockfd = sockfd;
}

std::unique_ptr<NetworkPacket> Network::recv_packet(){
    char version[2];
    char packet_len[2];
    char action[2];

    memset(version, 0, 2);
    memset(packet_len, 0, 2);
    memset(action, 0, 2);

    recv(this->sockfd, version, 2, MSG_WAITALL);
    recv(this->sockfd, packet_len, 2, MSG_WAITALL);
    recv(this->sockfd, action, 2, MSG_WAITALL);

    uint16_t version_i = this->ctou16(version);
    uint16_t packet_len_i = this->ctou16(packet_len);
    uint16_t action_i = this->ctou16(action);

    char *payload = reinterpret_cast<char*>(malloc(packet_len_i));
    if(packet_len_i > 0){
        memset(payload, 0, packet_len_i);
        recv(this->sockfd, payload, packet_len_i, MSG_WAITALL);
    }

    if(packet_len_i > 0){
        std::unique_ptr<NetworkPacket> np = std::make_unique<NetworkPacket>(action_i, packet_len_i, (char*)payload);
        return np;
    }

    std::unique_ptr<NetworkPacket> np = std::make_unique<NetworkPacket>(action_i);
    return np;
}

bool Network::send_packet(std::unique_ptr<NetworkPacket> np){
    printf("Printing frame version: %d\n", np->version);
    printf("Printing frame plength: %d\n", np->packet_len);
    printf("Printing frame paction: %d\n", np->action);

    auto version = reverse_short(np->u16toc(np->version));
    auto packet_len = reverse_short(np->u16toc(np->packet_len));
    auto action = reverse_short(np->u16toc(np->action));

    send(this->sockfd, version, 2, 0);
    send(this->sockfd, packet_len, 2, 0);
    send(this->sockfd, action, 2, 0);

    free(version);
    free(packet_len);
    free(action);

    if(np->packet_len > 1)
        send(this->sockfd, np->payload, np->packet_len, 0);
    return true;
}

uint16_t Network::ctou16(char data[]){
    return (uint16_t)data[1] | (uint16_t)data[0] << 8;
}

char* NetworkPacket::craft_packet(){
    char *payload = (char*)malloc(6+this->packet_len);
    strncpy(payload, this->u16toc(this->version), 2);
    strncpy(payload, this->u16toc(this->packet_len), 2);
    strncpy(payload, this->u16toc(this->action), 2);
    if(this->packet_len > 0)
        strncpy(payload, this->payload, this->packet_len);
    printf("%06X\n", *payload);
    return strdup(payload);
}

char* NetworkPacket::u16toc(uint16_t data){
    unsigned char let[2];
    let[0] = (data) & 0xFF;
    let[1] = data >> 8;
    printf("NetworkPacket::u16toc decoded: %04x Note that if the values are higher than 0x01 then they are corrupted\n", *let);
    return strdup(reinterpret_cast<char*>(let));
}

char* reverse_short (char *c) {
    char* p = reinterpret_cast<char*>(malloc(2));

    if (is_bigendian()) {
        p[0] = c[0];
        p[1] = c[1];
    } else {
        p[0] = c[1];
        p[1] = c[0];
    }
    return p;
}
