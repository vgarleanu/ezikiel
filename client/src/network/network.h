#ifndef NETWORK_H_
#define NETWORK_H_
#define PROTOCOL_VERSION 0x0001
#include <cstdint>
#include <cstddef>
#include <memory>

class NetworkPacket {
    public:
        NetworkPacket(uint16_t action) : action(action) {};
        NetworkPacket(uint16_t action, uint16_t packet_len, char *payload) : action(action), packet_len(packet_len), payload(payload) {};
        ~NetworkPacket() = default;

        char* craft_packet();
        char* u16toc(uint16_t data);

        uint16_t action;
        uint16_t packet_len = 0;
        uint16_t version = PROTOCOL_VERSION;
        size_t hdr_size = 6;
        char *payload;
};

class Network {
    public:
        explicit Network();
        Network(const Network&) = delete;
        Network& operator=(const Network&) = delete;
        ~Network() = default;

        std::unique_ptr<NetworkPacket> recv_packet();
        bool send_packet(std::unique_ptr<NetworkPacket> np);
        uint16_t ctou16(char data[]);

        int sockfd;
};

enum Actions{
    BETA,
    ANNOUNCE
};

char* reverse_short(char* c);

#endif
