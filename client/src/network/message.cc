#include "message.h"

std::unique_ptr<Message> Message::unpack_char(char* data){
    std::string data_str(data);

    msgpack::object_handle handle = msgpack::unpack(data_str.data(), data_str.size());
    msgpack::object deserialized = handle.get();

    // msgpack::object supports ostream.
    std::cout << deserialized << std::endl;

    // or create the new instance
    Message dst2 = deserialized.as<Message>();
    auto ptr2 = std::unique_ptr<Message>(&dst2);
    return ptr2;
}

char* Message::pack_char(){
    std::stringstream buffer;
    msgpack::pack(buffer, this->data);

    std::string unchar_buffer(buffer.str());
    char* ret_data = reinterpret_cast<char*>(malloc(unchar_buffer.length()));
    *ret_data = reinterpret_cast<char>(unchar_buffer.c_str());

    return ret_data;
}
