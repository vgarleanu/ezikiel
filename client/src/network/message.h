#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include <msgpack.hpp>
#include <sstream>

class Message {
    public:
        static std::unique_ptr<Message> unpack_char(char* data);
        char* pack_char();
        std::map<std::string, std::string> data;
    MSGPACK_DEFINE(data);
};
